//Nolan Ganz 2038533
package fall2021lab02.BicycleCode;

public class BikeStore {
    public static void main(String[] args)
    {
        Bicycle[] bikeStore = new Bicycle[4];
        bikeStore[0] = new Bicycle("Norco", 12, 40);
        bikeStore[1] = new Bicycle("BikeGuy", 4, 20);
        bikeStore[2] = new Bicycle("Norse", 10, 40);
        bikeStore[3] = new Bicycle("Norco", 6, 40);

        for (int i = 0; i < bikeStore.length; i++)
        {
            System.out.println(bikeStore[i]);
        }
    }
}
